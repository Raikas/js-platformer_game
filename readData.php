<html><body>
 <?php
$servername = "localhost";
$username = "joonakleemola";
$password = "";
$dbname = "highscores";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT name, time, levelName FROM score";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "Name: " . $row["name"]. " - Time: " . $row["time"]. " " . $row["levelName"]. "<br>";
    }
} else {
    echo "0 results";
}
$conn->close();
?> 
</body>
</html>